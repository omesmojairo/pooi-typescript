/*Criar uma classe EstatisticasDeSeries, que irá conter 4 métodos estáticos 
    obterDuracaoDaSerieEmMinutos, 
        que irá receber um objeto da classe Serie como parâmetro, percorrer o vetor de temporadas deste objeto, e dentro disso percorrer o vetor de episódios da temporada, 
        somar a duração dos episódios e retornar este valor;  
    obterDuracaoDaTemporadaEmMinutos, 
        que irá receber um objeto da classe Temporada como parâmetro, percorrer o vetor de episódios da temporada, somar a duração dos episódios e retornar este valor;         
    obterTotalDeTemporadas, 
        que irá receber um objeto da classe Serie como parâmetro e retornar o tamanho do vetor de temporadas;     
    obterTotalDeEpisodios, 
        que irá receber um objeto da classe Serie como parâmetro, percorrer o vetor de temporadas deste objeto, somar o tamanho dos vetores de episódios de cada temporada e retornar este valor.

*/

import { Episodio } from "./Episodio";
import { Serie } from "./Serie";
import { Temporada } from "./Temporada";

export class EstatisticasDeSeries {

    public static obterDuracaoDaSerieEmMinutos (temporadas:Serie):number{
        let duracaoTotal:number=0;
        for (let i=0; i< Temporada.length; i++){
            for (let j=0; j < Episodio.length; i++){
                duracaoTotal += Episodio.getDuracao();
            }
        }
    return duracaoTotal;
    }

    public static obterDuracaoTemporadaEmMinutos (duracao:Temporada):number{
        let duracaoTotal2:number=0;
        for (let i=0; i< Temporada.length; i++){
            for (let j=0; j < Episodio.length; i++){
                duracaoTotal2 += Episodio.getDuracao();
            }
        }
    return duracaoTotal2;
    }
    public static obterTotalDeTemporadas (temporada:Serie):number{
        let totalDeTemporadas:number=Temporada.length;
    return totalDeTemporadas;
    }
    
    public static obterTotalDeEpisodios (serie:Serie):number{
        let duracaoTotal3:number=0;
        for (let i=0; i< Temporada.length; i++){
            duracaoTotal3 += Episodio.length;    
        }
        return duracaoTotal3;
    }
    
}
