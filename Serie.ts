import { Obra } from "./Obra";
import { Personagem } from "./Personagem";
import { Temporada } from "./Temporada";

/*
 8 - Criar uma classe Serie, que será filha da classe Obra, com o seguinte atributo 
        temporadas (vetor de objetos da classe Temporada). 

Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado. 
Esta classe também deverá ter um método chamado 
    adicionarTemporada, que irá receber um objeto da classe Temporada como parâmetro e adicioná-lo ao vetor de temporadas. 

Por fim, esta classe deverá implementar o método abstrato 
        obterNota, retornando a média das notas das temporadas. 
*/
export class Serie extends Obra {           

    private temporadas:Temporada[];
       
    public constructor (temporadas:Temporada[], nome: string, personagens:Personagem[]){
        super(nome, personagens);
        this.temporadas=temporadas;
        
    }

    public getTemporadas():Temporada[] {                    //getters
        return this.temporadas;
    }
        
    public setTemporadas(temporadas:Temporada[]) {                  //setters
        this.temporadas=temporadas;
    }
   
    public adicionarTemporada(cast:Temporada) {    //método adicionarTemporada() 
        this.temporadas.push(cast);
    }
    
    public obterNota():number {      //método obterNota(), deve retornar a média das notas das temporadas 
        let notaTotal:number=0;         //recebe a soma total das notas
        for (let i=0; i< this.temporadas.length; i++){ 
            notaTotal += this.temporadas[i].getNota();
        }
        let notaMedia:number;
        notaMedia = notaTotal/this.temporadas.length;      //calcula nota media
        return notaMedia;
    }

    

}
