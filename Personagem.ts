import { Ator } from "./Ator";
import { TipoPersonagem } from "./TipoPersonagem";

/*
3 - Criar uma classe Personagem, com os seguintes atributos: 
    nomePersonagem, 
    tipo (respeitando a enum TipoPersonagem) 
    ator (objeto da classe Ator). 

    Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
    Além disso, a classe deverá ter o método construtor implementado.

*/
export class Personagem {           //herdou de Ator.ts 

    private nomePersonagem:string;
    private tipoPersonagem:TipoPersonagem;
    private ator:Ator;                                              

    public constructor (nomePersonagem:string, tipoPersonagem:TipoPersonagem, ator:Ator){
        this.nomePersonagem=nomePersonagem;
        this.tipoPersonagem=tipoPersonagem;
        this.ator=ator;
    }

    public getNomePersonagem():string {           //getters
        return this.nomePersonagem;
    }
    public getTipoPersonagem():TipoPersonagem {           //getters
        return this.tipoPersonagem;
    }
    public getAtor():Ator {           //getters
        return this.ator;
    }

    public setNomePersonagem(nomePersonagem:string) {
        this.nomePersonagem=nomePersonagem;          //setters
    }
    public setTipoPersonagem(tipoPersonagem:TipoPersonagem) {
        this.tipoPersonagem=tipoPersonagem;          //setters
    }
    public setAtor(ator:Ator) {
        this.ator=ator;          //setters
    }
  
    
}