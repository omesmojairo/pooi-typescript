
/*
6 - Criar uma classe abstrata Obra, com os seguintes atributos 
    nome e 
    personagens (vetor de objetos da classe Personagem). 
Para esta classe, todos os atributos deverão ser protegidos e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado. 
Por fim, esta classe deverá conter três métodos adicionais 
    adicionarPersonagem, que irá receber um objeto da classe Personagem como parâmetro e adicioná-lo ao vetor de personagens; 
    obterProtagonisas(), que irá percorrer o vetor de personagens e, se o tipo do personagem for protagonista, irá adicionar a um segundo vetor criado dentro do método, que será retornado posteriormente; e 
    obterNota, que será um método abstrato que não recebe parâmetros e retornará um valor numérico.

*/
import { Personagem } from "./Personagem";
import { TipoPersonagem } from "./TipoPersonagem";

export abstract class Obra {                    //classe abstrata nao pode ser instanciada     

    protected nome:string;
    protected personagens:Personagem[];     //importado de Personagem.ts
    
    
    public constructor (nome:string, personagens:Personagem[]){
        this.nome=nome;
        this.personagens=personagens;
    }

    public getNome():string {                    //getters
        return this.nome;
    }
        
    public setNome(nome:string) {                  //setters
        this.nome=nome;
    }
            
    public adicionarPersonagem(elenco:Personagem) {                //método adicionarEpisodio() 
        //let elenco: Personagem[] = [];         //criar um vetor vazio
        this.personagens.push(elenco);            
    }

    public obterProtagonistas():Personagem[]{ //deve retornar um vetor com todos personagens do tipo PROTAGONISTA
        let vetorProtagonistas:Personagem[]=[];             //vatiavel criada: vetorProtagonistas
        for (let i=0; i< this.personagens.length; i++){
            if(this.personagens[i].getTipoPersonagem() == TipoPersonagem.PROTAGONISTA) {
                vetorProtagonistas.push(this.personagens[i]);
            }
        }
        return vetorProtagonistas;
    }


    
    public abstract obterNota():number;              //implementar em Filme.ts
        

}
