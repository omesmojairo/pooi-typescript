import { Sexo } from "./Sexo";

/*
Criar uma classe Pessoa, com os seguintes atributos 
nome e 
sexo (do tipo Sexo, respeitando a Enum). 
Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado.

*/
export class Pessoa {

    private nome:string;
    private sexo:Sexo;                                              //herda atributos da classe Sexo.ts

    public constructor (nome:string, sexo:Sexo){
        this.nome=nome;
        this.sexo=sexo;
    }

    public getNome():string{
        return this.nome;
    }
    
    public setNome(nome:string){
        this.nome = nome;
    }
    public getSexo():Sexo{
        return this.sexo;
    }

    public setSexo (sexo:Sexo){
        this.sexo = sexo;
    }









}