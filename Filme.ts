import { Obra } from "./Obra";
import { Personagem } from "./Personagem";

/*
7 - Criar uma classe Filme, que será filha da classe Obra, com os seguintes atributos 
    duracao e 
    nota. 
Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado. 
Por fim, esta classe deverá implementar o método abstrato obterNota, retornando o valor do atributo nota.
*/
export class Filme extends Obra {           

    private duracao:number;
    private nota:number;
    

    public constructor (duracao:number, nota:number, nome: string, personagens:Personagem[]){
        super(nome, personagens);
        this.duracao=duracao;
        this.nota=nota;

    }

    public getNome():string {                    //getters
        return this.nome;
    }
        
    public setNome(nome:string) {                  //setters
        this.nome=nome;
    }
            
     
    public obterNota() {                //método obterNota()
        return this.nota;
    }

}
