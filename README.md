# POOI-typescript

Trabalho Final
  
Para este exercício, é recomendado utilizar a pasta modelo para projetos TS.
Dentro da pasta, deverão existir um arquivo para cada classe, bem como o arquivo de teste.

1. Criar uma enum TipoPersonagem, contendo os seguintes valores: PROTAGONISTA, COADJUVANTE, RECORRENTE, PARTICIPACAO_ESPECIAL.

2. Criar uma classe Ator, com os seguintes atributos: nome e idade. Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado.

3. Criar uma classe Personagem, com os seguintes atributos: nomePersonangem, tipo (respeitando a enum TipoPersonagem) ator (objeto da classe Ator). Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado.

4. Criar uma classe Episodio, com os seguintes atributos: numero, nome e duracao (em minutos). Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado.

5. Criar uma classe Temporada, com os seguintes atributos: numero, nota e episodios (vetor de objetos da classe Episodio). Para esta classe, 
todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado. Por fim, esta classe deverá ter um método chamado adicionarEpisodio, que irá receber um objeto da classe Episodio como parâmetro e adicioná-lo ao vetor de episódios.

6. Criar uma classe abstrata Obra, com os seguintes atributos: nome e personagens (vetor de objetos da classe Personagem). Para esta classe, todos os atributos deverão ser protegidos e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado. Por fim, esta classe deverá conter três métodos adicionais: adicionarPersonagem, que irá receber um objeto da classe Personagem como parâmetro e adicioná-lo ao vetor de personagens; obterProtagonisas(), que irá percorrer o vetor de personagens e, se o tipo do personagem for protagonista, irá adicionar a um segundo vetor criado dentro do método, que será retornado posteriormente; e obterNota, que será um método abstrato que não recebe parâmetros e retornará um valor numérico.

7. Criar uma classe Filme, que será filha da classe Obra, com os seguintes atributos: duracao e nota. Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado. Por fim, esta classe deverá implementar o método abstrato obterNota, retornando o valor dao atributo nota.

8. Criar uma classe Serie, que será filha da classe Obra, com o seguinte atributo: temporadas (vetor de objetos da classe Temporada). Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. Além disso, a classe deverá ter o método construtor implementado. Esta classe também deverá ter um método chamado adicionarTemporada, que irá receber um objeto da classe Temporada como parâmetro e adicioná-lo ao vetor de temporadas. Por fim, esta classe deverá implementar o método abstrato obterNota, retornando a média das notas das temporadas.

9. Criar uma classe EstatisticasDeSeries, que irá conter 4 métodos estáticos: obterDuracaoDaSerieEmMinutos, que irá receber um objeto da classe Serie como parâmetro, percorrer o vetor de temporadas deste objeto, e dentro disso percorrer o vetor de episódios da temporada, somar a duração dos episódios e retornar este valor; obterDuracaoDaTemporadaEmMinutos, que irá receber um objeto da classe Temporada como parâmetro, percorrer o vetor de episódios da temporada, somar a duração dos episódios e retornar este valor; obterTotalDeTemporadas, que irá receber um objeto da classe Serie como parâmetro e retornar o tamanho do vetor de temporadas; obterTotalDeEpisodios, que irá receber um objeto da classe Serie como parâmetro, percorrer o vetor de temporadas deste objeto, somar o tamanho dos vetores de episódios de cada temporada e retornar este valor.

10. Criar uma classe de teste para trabalhar com as classes criadas acima.
