/*
Criar uma classe Ator, com os seguintes atributos 
    nome e 
    idade. 
Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado

*/
export class Ator {

    private nome:string;
    private idade:number;                                              

    public constructor (nome:string, idade:number){
        this.nome=nome;
        this.idade=idade;
    }

    public getNome():string{        //getters
        return this.nome;
    }
    
    public getIdade():number{
        return this.idade;
    }
    
    public setNome(nome:string){    //setters
        this.nome = nome;
    }
    
    public setIdade (idade:number){
        this.idade = idade;
    }



}