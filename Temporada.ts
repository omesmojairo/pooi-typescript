import { Episodio } from "./Episodio";
/*
5 - Criar uma classe Temporada, com os seguintes atributos 
    numero, 
    nota e 
    episodios (vetor de objetos da classe Episodio). 
Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado. 
Por fim, esta classe deverá ter um método chamado adicionarEpisodio, 
que irá receber um objeto da classe Episodio como parâmetro e adicioná-lo ao vetor de episódios.

*/
export class Temporada {                    //ver osdemDeServico e Servico, aqui será episodios e temporadas

    private numero:number;
    private nota:number;
    private episodios:Episodio[];     //importado de Episodio.ts (numero, nome, duracao)
    
    public constructor (numero:number, nota:number, episodios:[Episodio]){
        this.numero=numero;
        this.nota=nota;
        this.episodios=[];
    }

    public getNumero():number {                    //getters
        return this.numero;
    }
    public getNota():number {                      //getters 
        return this.nota;
    }
    public getEpisodios():Episodio[] {                
        return this.episodios;

    }
    public setEpisodios(episodios:Episodio[]) {      
        this.episodios=episodios;

    }
    
    public setNumero(numero:number) {                  //setters
        this.numero=numero;
    }
    public setNota(nota:number) {                      //setters 
        this.nota=nota;
    }
        
    public adicionarEpisodio(episodio:Episodio) {    //método adicionarEpisodio() 
        this.episodios.push(episodio);
        //let episodios: Episodio[] = [];         //criar um vetor vazio
        //episodios.push(assistir);               //assistir será um parametro do index.ts
    }
    
}
