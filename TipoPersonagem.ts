/*
    Criar uma enum TipoPersonagem, contendo os seguintes valores 
    PROTAGONISTA, 
    COADJUVANTE,
    RECORRENTE, 
    PARTICIPACAO_ESPECIAL.

*/

export enum TipoPersonagem {
    PROTAGONISTA,
    COADJUVANTE,
    RECORRENTE,
    PARTICIPACAO_ESPECIAL
}