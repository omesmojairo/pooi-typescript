import { Ator } from "./Ator";
/*
4 -Criar uma classe Episodio, com os seguintes atributos 
    numero, 
    nome e 
    duracao (em minutos). 
Para esta classe, todos os atributos deverão ser privados e, para acessá-los, os getters e setters também devem ser implementados. 
Além disso, a classe deverá ter o método construtor implementado.
*/
export class Episodio {           

    private numero:number;
    private nome:string;
    private duracao:number;                                              

    public constructor (numero:number, nome:string, duracao:number){
        this.numero=numero;
        this.nome=nome;
        this.duracao=duracao;
    }

    public getNumero():number {                    //getters
        return this.numero;
    }
    public getNome():string {                      //getters 
        return this.nome;
    }
    public getDuracao():number {                   //getters
        return this.duracao;
    }
    
    public setNumero(numero:number) {                    //setters
        this.numero=numero;
    }
    public setNome(nome:string) {                      //setters 
        this.nome=nome;
    }
    public setDuracao(duracao:number) {                   //setters
        this.duracao=duracao;
    }

    
}